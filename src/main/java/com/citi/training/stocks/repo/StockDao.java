package com.citi.training.stocks.repo;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.stocks.model.Stock;

public interface StockDao extends CrudRepository<Stock, Long>{

}
